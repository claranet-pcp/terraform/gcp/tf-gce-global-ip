output "name" {
  value = "${google_compute_global_address.ip_address.name}"
}
output "ip_address" {
  value = "${google_compute_global_address.ip_address.address}"
}
output "link" {
  value = "${google_compute_global_address.ip_address.self_link}"
}
