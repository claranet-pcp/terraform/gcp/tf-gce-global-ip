variable "service" {
  description = "The name of the service, will become the suffix for the global IP name"
  type = "string"
}
